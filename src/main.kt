//თქვენი შეხედულებისამებრ, მოცემულ პროექტში დააასრულეთ Abstract factory პატერნის იმპლემენტაცია და მოიყვაეთ გამოყენების მაგალითი.
fun main(){
    val abstractclass = AnimalFactory()
    println(abstractclass.create("Cat")?.pet())

}