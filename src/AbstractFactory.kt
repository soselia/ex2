interface Animal {
    fun pet();
}


class Dog : Animal{
    override fun pet() {
        println("woh woh")
    }
}

class Cat : Animal{
    override fun pet() {
        println("miau")
    }
}

class AnimalFactory {
    fun create(type: String):Animal?{
        return when(type){
            "Dog" ->  Dog()
            "Cat" ->  Cat()
            else   -> null
        }
    }
}

